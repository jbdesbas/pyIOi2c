class PCF8574:
    from smbus import SMBus
    def __init__(self,adr,bus):
        self.adr=adr
        self.bus=bus
        self.trame=0b00000000
        self.HIGH=True #Alias
        self.LOW=False #Alias
    
    def write(self,pin,etat=None):
        if (type(pin) is not int) or (pin > 7):
            raise ValueError("write(int pin,[bool etat]). 0 <= pin <= 7 ")
        if etat==True:
            self.trame=self.trame | (0x01 << pin)
        elif etat==False:
            self.trame=self.trame & (0xFF ^ (0x01<<pin))
        elif etat is None:
            self.trame=self.trame ^ (0x01 << pin) #Toggle
        else:
            raise ValueError("write(int pin,[bool etat])")
        return self.trame

